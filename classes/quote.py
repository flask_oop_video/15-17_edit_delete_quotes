# import needed modules
import sqlite3
from config import Config

# create a config object so we can access it's properties
config = Config()

# ---------------------------------------------


class Quote:

    # this is a contructor class
    # it holds properties of the object that can be 
    # referred to throughout via "self"
    def __init__(self):
        # put properties here

        # path is relative to root of app (app.py)
        # it comes from the config.py class in the root folder
        self.dbPath = config.dbName

    # this method gets all of the quotes
    def retrieveAllQuotes(self):

        with sqlite3.connect(self.dbPath) as con:

            # this makes the results come back as a dictionary
            con.row_factory = sqlite3.Row 
            # create cursor
            cur = con.cursor()
            # SQL statement with sort by descending order
            cur.execute("SELECT * FROM quotes ORDER BY timeSubmitted DESC")
            # get the results
            result = cur.fetchall()
            # close our connection
            cur.close()

            # if the result exists then return it to app.py else return False
            if result:
                return result
            else:
                return False


    # this method retrieves just the users quotes
    def retrieveUserQuotes(self,id_user):

        with sqlite3.connect(self.dbPath) as con:
 
            # this makes the results come back as a dictionary
            con.row_factory = sqlite3.Row 
            # create cursor
            cur = con.cursor()
            # SQL statement with sort by descending order
            cur.execute("SELECT * FROM quotes WHERE id_user = ? ORDER BY timeSubmitted DESC",[id_user])
            # get the results
            result = cur.fetchall()
            # close our connection
            cur.close()

            # if the result exists then return it to app.py else return False
            if result:
                return result
            else:
                return False
           
    def retrieveSingleQuote(self,quoteId,id_user):

        with sqlite3.connect(self.dbPath) as con:

            # this makes the results come back as a dictionary
            con.row_factory = sqlite3.Row 
            # create cursor
            cur = con.cursor()
            # SQL statement with sort by descending order
            cur.execute("SELECT * FROM quotes WHERE id = ? AND id_user = ?",[quoteId,id_user])
            # get the results
            result = cur.fetchone()
            # close our connection
            cur.close()

            if result:
                return result
            else:
                return False


    # this method addQuote lets us insert a quote into the database
    def addQuote(self,quote,source,id_users):

        # this is a convenient way to open and close 
        # a database connection. Notice how we've used the self.dbPath
        # property of the user object
        with sqlite3.connect(self.dbPath) as con:

            # create cursor
            cur = con.cursor()
            # execute SQL. Notice how we've used ? as placeholders for the actual values. 
            # This is important to prevent SQL injection. The values we want to enter
            # into the database are given as a set in the exact order immediately after.
            cur.execute("INSERT INTO quotes (quote,source,id_user) VALUES (?,?,?)",(quote,source,id_users))
            # this commits the row into the quotes table
            con.commit()
            cur.close()
            # now we return True to app.py so that we can handle errors/success
            return True





    def deleteQuote(self,id_user,quoteId):

        with sqlite3.connect(self.dbPath) as con:

            cur = con.cursor()
            cur.execute("SELECT * FROM quotes WHERE id_user = ? AND id = ?",[id_user,quoteId])
            result = cur.fetchone()

            if result:
                cur.execute("DELETE FROM quotes WHERE id = ?",[quoteId])
                cur.close()
                return True
            else:
                cur.close()
                return False



    def editQuote(self,quoteId,quoteData,sourceData,id_user):

        with sqlite3.connect(self.dbPath) as con:

            cur = con.cursor()
            cur.execute("SELECT * FROM quotes WHERE id_user = ? AND id = ?",[id_user,quoteId])
            result = cur.fetchone()

            if result:
                cur.execute("UPDATE quotes SET quote=?, source=? WHERE id = ?",[quoteData,sourceData,quoteId])
                cur.close()
                return True
            else:
                cur.close()
                return False
